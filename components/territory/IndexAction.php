<?php

namespace app\components\territory;

use yii\rest\Action;
use app\models\Territory;

class IndexAction extends Action
{

    public function run()
    {
        return (new Territory())->getList();
    }

}
