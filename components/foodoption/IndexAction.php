<?php

namespace app\components\foodoption;

use yii\rest\Action;
use app\models\Foodoption;

class IndexAction extends Action
{

    public function run()
    {
        return (new Foodoption())->getList();
    }
}
