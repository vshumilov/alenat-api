<?php

namespace app\components\organization;

use yii\rest\Action;
use app\models\Organization;
use Yii;

class IndexAction extends Action
{

    public function run()
    {
        return Organization::find()->limit(1)->asArray()->one();
    }

}
