<?php

namespace app\components\pdfAgreement;

use yii\rest\Action;
use app\components\helpers\GuidHelper;
use app\components\helpers\PhoneHelper;
use yii\web\ServerErrorHttpException;
use Yii;

class ViewAction extends Action
{

    public function run()
    {
        $agreementId = Yii::$app->getRequest()->get('id');
        $code = Yii::$app->getRequest()->getQueryParam('code');

        $guidHelper = new GuidHelper();

        if (!$guidHelper->validate($agreementId)) {
            throw new ServerErrorHttpException('agreementId is not valid');
        }

        $phoneHelper = new PhoneHelper();
        if (!$phoneHelper->isValidCode($code)) {
            throw new ServerErrorHttpException('Code is not valid');
        }

        Yii::$app->api->auth();

        $params = [];
        $params['entryPoint'] = 'ajax';
        $params['module'] = 'Opportunities';
        $params['call'] = 'getPdfAgreement';
        $params['params[opportunity_id]'] = $agreementId;
        $params['params[code]'] = $code;

        $result = json_decode(Yii::$app->api->get("index.php", $params), true);

        if (isset($result['error'])) {
            throw new ServerErrorHttpException($result['error'][0]);
        }

        return $result;
    }

}
