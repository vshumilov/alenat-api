<?php

namespace app\components\agreement;

use yii\rest\Action;
use app\components\helpers\GuidHelper;
use app\components\helpers\PhoneHelper;
use app\components\helpers\EmailHelper;
use app\components\helpers\DateHelper;
use yii\web\ServerErrorHttpException;
use Yii;

class CreateAction extends Action
{

    public function run()
    {
        $priceToApartmentId = Yii::$app->getRequest()->getBodyParam('price_to_apartment_id');
        $dateFrom = Yii::$app->getRequest()->getBodyParam('date_from');
        $dateTo = Yii::$app->getRequest()->getBodyParam('date_to');
        $countPlaces = Yii::$app->getRequest()->getBodyParam('count_places');
        $countPlacesWithTreatment = Yii::$app->getRequest()->getBodyParam('count_places_with_treatment');
        $lastName = Yii::$app->getRequest()->getBodyParam('last_name');
        $firstName = Yii::$app->getRequest()->getBodyParam('first_name');
        $middleName = Yii::$app->getRequest()->getBodyParam('middle_name');
        $birthDate = Yii::$app->getRequest()->getBodyParam('birth_date');
        $email = Yii::$app->getRequest()->getBodyParam('email');
        $phoneMobile = Yii::$app->getRequest()->getBodyParam('phone_mobile');

        if (empty($priceToApartmentId) ||
                empty($dateFrom) ||
                empty($dateTo) ||
                !isset($countPlaces) ||
                !isset($countPlacesWithTreatment) ||
                empty($lastName) ||
                empty($firstName) ||
                empty($middleName) ||
                empty($birthDate) ||
                empty($email) ||
                empty($phoneMobile)) {
            throw new ServerErrorHttpException('Some of required params are empty');
        }

        $guidHelper = new GuidHelper();

        if (!$guidHelper->validate($priceToApartmentId)) {
            throw new ServerErrorHttpException('priceToApartmentId is not valid');
        }
        
        $dateHelper = new DateHelper();
        //TODO: check if $dateFrom > $dateTo
        if (!$dateHelper->validate($dateFrom)) {
            throw new ServerErrorHttpException('dateFrom is not valid');
        }
        
        if (!$dateHelper->validate($dateTo)) {
            throw new ServerErrorHttpException('dateTo is not valid');
        }
        
        //TODO: check if $birthDate is adult
        if (!$dateHelper->validate($birthDate)) {
            throw new ServerErrorHttpException('birthDate is not valid');
        }

        if (!($countPlaces > 0 || $countPlacesWithTreatment > 0)) {
            throw new ServerErrorHttpException('countPlaces + countPlacesWithTreatment = 0');
        }

        $phoneHelper = new PhoneHelper();

        if (!$phoneHelper->isMobilePhone($phoneMobile)) {
            throw new ServerErrorHttpException('Phone is not valid mobile phone');
        }

        $phoneMobile = $phoneHelper->format($phoneMobile);

        $emailHelper = new EmailHelper();

        if (!$emailHelper->validate($email)) {
            throw new ServerErrorHttpException('Email is not valid');
        }

        Yii::$app->api->auth();

        $params = [];
        $params['entryPoint'] = 'ajax';
        $params['module'] = 'Tours';
        $params['call'] = 'create';
        $params['params[price_to_apartment_id]'] = $priceToApartmentId;
        $params['params[date_from]'] = $dateFrom;
        $params['params[date_to]'] = $dateTo;
        $params['params[count_places]'] = $countPlaces;
        $params['params[count_places_with_treatment]'] = $countPlacesWithTreatment;
        $params['params[last_name]'] = $lastName;
        $params['params[first_name]'] = $firstName;
        $params['params[middle_name]'] = $middleName;
        $params['params[birth_date]'] = $birthDate;
        $params['params[phone_mobile]'] = $phoneMobile;
        $params['params[email]'] = $email;

        $result = json_decode(Yii::$app->api->get("index.php", $params), true);
        
        if (isset($result['error'])) {
            throw new ServerErrorHttpException($result['error'][0]);
        }

        return $result;
    }

}
