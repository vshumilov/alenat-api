<?php

namespace app\components\agreement;

use yii\rest\Action;
use app\components\helpers\GuidHelper;
use app\components\helpers\PhoneHelper;
use yii\web\ServerErrorHttpException;
use Yii;

class ViewAction extends Action
{

    public function run()
    {
        $agreementId = Yii::$app->getRequest()->get('id');
        $onlyShortData = Yii::$app->getRequest()->getQueryParam('onlyShortData');
        $code = Yii::$app->getRequest()->getQueryParam('code');

        $guidHelper = new GuidHelper();

        if (!$guidHelper->validate($agreementId)) {
            throw new ServerErrorHttpException('agreementId is not valid');
        }

        Yii::$app->api->auth();

        $params = [];
        $params['entryPoint'] = 'ajax';
        $params['module'] = 'Opportunities';
        $params['call'] = 'getById';
        $params['params[opportunity_id]'] = $agreementId;

        if (!empty($onlyShortData)) {
            $params['params[only_short_data]'] = true;
        } else {
            $phoneHelper = new PhoneHelper();

            if (!$phoneHelper->isValidCode($code)) {
                throw new ServerErrorHttpException('Code is not valid');
            }
            
            $params['params[code]'] = $code;
        }

        $result = json_decode(Yii::$app->api->get("index.php", $params), true);

        if (isset($result['error'])) {
            throw new ServerErrorHttpException($result['error'][0]);
        }

        return $result;
    }

}
