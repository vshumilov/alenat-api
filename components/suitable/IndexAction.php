<?php

namespace app\components\suitable;

use yii\rest\Action;
use app\models\Suitable;

class IndexAction extends Action
{

    public function run()
    {
        return (new Suitable())->getList();
    }

}
