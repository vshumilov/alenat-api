<?php

namespace app\components\city;

use yii\rest\Action;
use app\models\City;

class IndexAction extends Action
{

    public function run()
    {
        $name = \Yii::$app->getRequest()->get('q');

        return (new City())->getList($name);
    }

}
