<?php

namespace app\components\sanatorium;

use yii\rest\Action;
use app\models\Sanatorium;

class ViewAction extends Action
{

    public function run()
    {
        $intId = \Yii::$app->getRequest()->get('id');

        return (new Sanatorium())->getByIntId($intId);
    }
}
