<?php

namespace app\components\sanatorium;

use yii\rest\Action;
use app\models\Sanatorium;

class SearchAction extends Action
{

    public function run()
    {
        $params = \Yii::$app->getRequest()->post();

        return (new Sanatorium())->search($params);
    }
}
