<?php

namespace app\components\arrival;

use yii\rest\Action;
use app\models\Arrival;

class IndexAction extends Action
{

    public function run()
    {
        return (new Arrival())->getList();
    }

}
