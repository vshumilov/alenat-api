<?php

namespace app\components\pricelevel;

use yii\rest\Action;
use app\models\Pricelevel;

class IndexAction extends Action
{

    public function run()
    {
        return (new Pricelevel())->getList();
    }

}
