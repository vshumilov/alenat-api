<?php

namespace app\components\entertainment;

use yii\rest\Action;
use app\models\Entertainment;

class IndexAction extends Action
{

    public function run()
    {
        return (new Entertainment())->getList();
    }

}
