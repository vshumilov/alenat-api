<?php

namespace app\components\country;

use yii\rest\Action;
use app\models\Country;

class IndexAction extends Action
{

    public function run()
    {
        $name = \Yii::$app->getRequest()->get('q');

        return (new Country())->getList($name);
    }

}
