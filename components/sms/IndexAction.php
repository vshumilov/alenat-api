<?php

namespace app\components\sms;

use yii\rest\Action;
use app\components\helpers\PhoneHelper;
use yii\web\ServerErrorHttpException;
use Yii;

class IndexAction extends Action
{

    public function run()
    {
        $code = Yii::$app->getRequest()->getQueryParam('code');
        $phone = Yii::$app->getRequest()->getQueryParam('phone');
        
        $phoneHelper = new PhoneHelper();
        
        if (!$phoneHelper->isValidCode($code)) {
            throw new ServerErrorHttpException('Code is not valid');
        }
        
        if (!$phoneHelper->isMobilePhone($phone)) {
            throw new ServerErrorHttpException('Phone is not valid mobile phone');
        }
        
        $phone = $phoneHelper->format($phone);
        
        Yii::$app->api->auth();
        
        $params = [];
        $params['entryPoint'] = 'ajax';
        $params['module'] = 'Dispatches';
        $params['call'] = 'checkSmsCode';
        $params['params[phone]'] = $phone;
        $params['params[code]'] = $code;
        
        $result = Yii::$app->api->get("index.php", $params);
        
        if (isset($result['error'])) {
            throw new ServerErrorHttpException($result['error'][0]);
        }
        
        return $result;
    }

}
