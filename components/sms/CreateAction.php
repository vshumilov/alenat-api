<?php

namespace app\components\sms;

use yii\rest\Action;
use app\components\helpers\PhoneHelper;
use yii\web\ServerErrorHttpException;
use Yii;

class CreateAction extends Action
{

    public function run()
    {
        $phone = Yii::$app->getRequest()->getBodyParam('phone');
        
        $phoneHelper = new PhoneHelper();
        
        if (!$phoneHelper->isMobilePhone($phone)) {
            throw new ServerErrorHttpException('Phone is not valid mobile phone');
        }
        
        $phone = $phoneHelper->format($phone);
        
        Yii::$app->api->auth();
        
        $params = [];
        $params['entryPoint'] = 'ajax';
        $params['module'] = 'Dispatches';
        $params['call'] = 'sendSmsCode';
        $params['params[phone]'] = $phone;
        
        $result = Yii::$app->api->get("index.php", $params);
        
        if (isset($result['error'])) {
            throw new ServerErrorHttpException($result['error'][0]);
        }
        
        return $result;
    }

}
