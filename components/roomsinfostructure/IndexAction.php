<?php

namespace app\components\roomsinfostructure;

use yii\rest\Action;
use app\models\Roomsinfostructure;

class IndexAction extends Action
{

    public function run()
    {
        return (new Roomsinfostructure())->getList();
    }

}
