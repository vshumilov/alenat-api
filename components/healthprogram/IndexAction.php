<?php

namespace app\components\healthprogram;

use yii\rest\Action;
use app\models\Healthprogram;

class IndexAction extends Action
{

    public function run()
    {
        return (new Healthprogram())->getList();
    }

}
