<?php

namespace app\components\helpers;

class DateHelper
{

    public function dbDateToHumanDate($dbDate)
    {
        $date = new \DateTime($dbDate);
        return $date->format('d.m.Y');
    }
    
    public function validate($dbDate) 
    {
        $date = new \DateTime($dbDate);
        
        return checkdate($date->format('m'), $date->format('d'), $date->format('Y'));
    }
}
