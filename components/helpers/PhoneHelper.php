<?php

namespace app\components\helpers;

class PhoneHelper
{

    public $phoneMaxLength = 10;
    public $codeLength = 4;

    public function format($phone, $prefix = '+7')
    {
        if (empty($phone)) {
            return;
        }
        
        $phone = trim($phone);

        $phone = strtr($phone, [
            '(' => '',
            ')' => '',
            ' ' => '',
            '+' => '',
            '-' => '',
        ]);

        $phoneLength = strlen($phone);

        if ($phoneLength < $this->phoneMaxLength) {
            return $phone;
        }

        $phone = substr($phone, $phoneLength - $this->phoneMaxLength, $this->phoneMaxLength);

        return $prefix . $phone;
    }
    
    public function isMobilePhone($phone)
    {
        $phone = $this->format($phone, "");
        
        if (empty($phone)) {
            return false;
        }
        
        if (strlen($phone) === $this->phoneMaxLength) {
            return true;
        }
        
        return false;
    }

    public function isValidCode($code)
    {
        if (empty($code) || strlen($code) != $this->codeLength || !is_numeric($code)) {
            return false;
        }
        
        return true;
    }
}
