<?php

namespace app\components\image;

use yii\rest\Action;
use app\models\Image;
use Yii;

class IndexAction extends Action
{

    public function run()
    {
        $parentId = Yii::$app->getRequest()->getQueryParam('parent_id');
        
        return (new Image())->getList($parentId);
    }

}
