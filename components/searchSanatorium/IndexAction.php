<?php

namespace app\components\searchSanatorium;

use yii\rest\Action;
use app\models\Beauty;
use app\models\Arrival;
use app\models\Entertainment;
use app\models\Foodoption;
use app\models\Healthprogram;
use app\models\Internet;
use app\models\Pricelevel;
use app\models\Rent;
use app\models\Roomsinfostructure;
use app\models\Sport;
use app\models\Suitable;
use app\models\Territory;
use app\models\Treatment;

class IndexAction extends Action
{

    public function run()
    {
        $searchFields = [];
        
        $searchFields['arrivals'] = (new Arrival())->getList();
        $searchFields['beautys'] = (new Beauty())->getList();
        $searchFields['entertainments'] = (new Entertainment())->getList();
        $searchFields['for_childrens'] = (new Entertainment())->getList(['is_for_children' => '1']);
        $searchFields['foodoptions'] = (new Foodoption())->getList();
        $searchFields['healthprograms'] = (new Healthprogram())->getList();
        $searchFields['internets'] = (new Internet())->getList();
        $searchFields['pricelevels'] = (new Pricelevel())->getList();
        $searchFields['rents'] = (new Rent())->getList();
        $searchFields['roomsinfostructures'] = (new Roomsinfostructure())->getList();
        $searchFields['sports'] = (new Sport())->getList();
        $searchFields['suitables'] = (new Suitable())->getList();
        $searchFields['territorys'] = (new Territory())->getList();
        $searchFields['treatments'] = (new Treatment())->getList();
        
        return $searchFields;
    }

}
