<?php

namespace app\components\rent;

use yii\rest\Action;
use app\models\Rent;

class IndexAction extends Action
{

    public function run()
    {
        return (new Rent())->getList();
    }

}
