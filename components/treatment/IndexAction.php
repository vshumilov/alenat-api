<?php

namespace app\components\treatment;

use yii\rest\Action;
use app\models\Treatment;

class IndexAction extends Action
{

    public function run()
    {
        return (new Treatment())->getList();
    }

}
