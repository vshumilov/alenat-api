<?php

namespace app\components\internet;

use yii\rest\Action;
use app\models\Internet;

class IndexAction extends Action
{

    public function run()
    {
        return (new Internet())->getList();
    }

}
