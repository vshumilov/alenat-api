<?php

namespace app\components\priceToApartment;

use yii\rest\Action;
use app\models\PriceToApartment;

class ViewAction extends Action
{

    public function run()
    {
        $id = \Yii::$app->getRequest()->get('id');

        return (new PriceToApartment())->getById($id);
    }
}
