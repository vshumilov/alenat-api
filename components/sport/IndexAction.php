<?php

namespace app\components\sport;

use yii\rest\Action;
use app\models\Sport;

class IndexAction extends Action
{

    public function run()
    {
        return (new Sport())->getList();
    }

}
