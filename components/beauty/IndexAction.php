<?php

namespace app\components\beauty;

use yii\rest\Action;
use app\models\Beauty;

class IndexAction extends Action
{

    public function run()
    {
        return (new Beauty())->getList();
    }

}
