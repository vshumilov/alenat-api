<?php

namespace app\components\api;

use Yii;
use yii\base\Component;
use app\models\AlenatCrmApi;

/**
 * Class Centerex
 *
 * @property string $url
 *
 * @package app\components
 */
class Api extends Component
{

    public $token = null;
    public $url = null;

    public function init()
    {
        $this->url = \Yii::$app->params['api_url'];
    }
    
    public function auth()
    {
        $params = [];
        $params['module'] = 'Users';
        $params['action'] = 'Authenticate';
        $params['user_name'] = \Yii::$app->params['api_login'];
        $params['user_password'] = \Yii::$app->params['api_password'];
        
        $result = \Yii::$app->api->post("index.php", $params);
        
        return $result;
    }        

    /**
     * 
     * @return array
     */
    public function get($action, $data = [])
    {
        if (!Yii::$app->user->isGuest) {
            $data['access-token'] = Yii::$app->user->identity->accessToken;
        }
        
        return AlenatCrmApi::i($this->url, $this->token)->get($action, $data);
    }

    public function post($action, $data = [], $errorCode = false, $buildQuery = true)
    {
        if (!Yii::$app->user->isGuest) {
            $data['access-token'] = Yii::$app->user->identity->accessToken;
        }
        return AlenatCrmApi::i($this->url, $this->token)->post($action, $data, $errorCode, $buildQuery);
    }

    public function put($action, $data = [], $errorCode = true)
    {
        if (!Yii::$app->user->isGuest) {
            $data['access-token'] = Yii::$app->user->identity->accessToken;
        }
        return AlenatCrmApi::i($this->url, $this->token)->put($action, $data, $errorCode);
    }

    public function delete($action, $data = [])
    {
        if (!Yii::$app->user->isGuest) {
            $data['access-token'] = Yii::$app->user->identity->accessToken;
        }
        return AlenatCrmApi::i($this->url, $this->token)->delete($action, $data);
    }

    public function arrayToUrl($data)
    {
        if (!Yii::$app->user->isGuest) {
            $data['access-token'] = Yii::$app->user->identity->accessToken;
        }
        return AlenatCrmApi::i($this->url, $this->token)->arrayToUrl($data);
    }

}
