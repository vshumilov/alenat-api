<?php

$db = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=alenat2',
    'username' => 'root',
    'password' => 'sa123456',
    'charset' => 'utf8',
    // Duration of schema cache.
    'schemaCacheDuration' => 3600,
    // Name of the cache component used to store schema information
    'schemaCache' => 'cache',
];

require(__DIR__ . '/local.db.php');

return $db;
