<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'api' => [
            'class' => 'app\components\api\Api',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'enableSession' => false,
            'loginUrl' => null
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => [
                        'client', 'auth', 'sanatorium', 'country', 'city', 'treatment',
                        'suitable', 'pricelevel', 'arrival', 'healthprogram', 'beauty',
                        'foodoption', 'roomsinfostructure', 'entertainment', 'territory',
                        'internet', 'rent', 'sport', 'search-sanatorium', 'image', 'sms',
                        'agreement', 'code-agreement', 'pdf-agreement', 'organization'
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'price-to-apartment',
                        'agreement',
                        'code-agreement',
                        'pdf-agreement'
                    ],
                    'pluralize' => true,
                    'tokens' => [
                        '{id}' => '<id:\\w+-\\w+-\\w+-\\w+-\\w+>'
                    ]
                ],
                'OPTIONS prices-apartments/<id:\s+>' => 'price-to-apartment/options',
            ],
        ],
    ],
    'params' => $params,
];

require(__DIR__ . '/local.php');

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
