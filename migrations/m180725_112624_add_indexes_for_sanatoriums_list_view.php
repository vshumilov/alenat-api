<?php

use yii\db\Migration;
use app\models\Sanatorium;

class m180725_112624_add_indexes_for_sanatoriums_list_view extends Migration
{

    public static $fields = [
        'treatment',
        'suitable',
        'healthprogram',
        'beauty',
        'foodoption',
        'roomsinfostructure',
        'entertainment',
        'territory',
        'internet'
    ];

    public function safeUp()
    {
        $sql = "
            ALTER TABLE <tableName> ADD INDEX idx_<columnName> (<columnName>(100))
        ";

        foreach (self::$fields as $field) {
            $query = strtr($sql, [
                '<tableName>' => Sanatorium::tableName(),
                '<columnName>' => $field
            ]);

            $this->db->createCommand()->setSql($query)->execute();
        }

        return true;
    }

    public function safeDown()
    {
        $sql = "
            DROP INDEX idx_<columnName> ON <tableName>
        ";

        foreach (self::$fields as $field) {
            $query = strtr($sql, [
                '<tableName>' => Sanatorium::tableName(),
                '<columnName>' => $field
            ]);

            $this->db->createCommand()->setSql($query)->execute();
        }

        return true;
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m180725_112624_add_indexes_for_sanatoriums_list_view cannot be reverted.\n";

      return false;
      }
     */
}
