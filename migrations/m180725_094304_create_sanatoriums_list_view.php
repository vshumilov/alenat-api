<?php

use yii\db\Migration;
use app\models\SanatoriumsList;

class m180725_094304_create_sanatoriums_list_view extends Migration
{
    public function safeUp()
    {
        $sql = "
            CREATE VIEW <viewTableName> AS 
                SELECT
                    p.date_from,
                    p.date_to,
                    c.name as city,
                    ct.name as country,
                    s.name,
                    s.treatment,
                    s.suitable,
                    s.healthprogram,
                    s.beauty,
                    s.foodoption,
                    s.roomsinfostructure,
                    s.entertainment,
                    s.territory,
                    s.internet,
                    s.id,
                    s.int_id,
                    s.country_id,
                    s.city_id,
                    p.cost,
                    p.cost_with_treatment,
                    p.foodoption_id,
                    i.name AS image,
                    i.thumbnail_name_s
                FROM 
                    sanatoriums AS s
                JOIN
                    prices AS p 
                ON
                    s.id = p.parent_id AND p.deleted = '0'
                JOIN
                    images AS i
                ON
                    s.id = i.parent_id AND i.deleted = '0' AND i.is_avatar = '1'  
                JOIN
                    countries AS ct
                ON
                    s.country_id = ct.id AND ct.deleted = '0'
                JOIN
                    cities AS c
                ON
                    s.city_id = c.id AND c.deleted = '0' 
                JOIN
                    prices_houses AS ph
                ON
                    p.id = ph.price_id AND ph.deleted = '0' 
                JOIN
                    houses AS h
                ON
                    ph.house_id = h.id AND h.deleted = '0'    
                WHERE
                    s.deleted = '0'
                ORDER BY
                    p.date_from, p.cost
                    
        ";
        
        $query = strtr($sql, ['<viewTableName>' => SanatoriumsList::tableName()]);
        
        $this->db->createCommand()->setSql($query)->execute();
        
        return true;
    }

    public function safeDown()
    {
        $sql = "DROP VIEW <viewTableName>";
        
        $query = strtr($sql, ['<viewTableName>' => SanatoriumsList::tableName()]);
        
        $this->db->createCommand()->setSql($query)->execute();

        return true;
    }

}
