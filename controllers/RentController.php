<?php

namespace app\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

class RentController extends ActiveController
{

    public $modelClass = 'app\models\Rent';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => 'app\components\rent\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
