<?php

namespace app\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

class FoodoptionController extends ActiveController
{

    public $modelClass = 'app\models\Foodoption';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => 'app\components\foodoption\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
