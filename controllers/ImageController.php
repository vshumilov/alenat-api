<?php

namespace app\controllers;

use yii\rest\ActiveController;

class ImageController extends ActiveController
{

    public $modelClass = 'app\models\Image';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => 'app\components\image\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}

