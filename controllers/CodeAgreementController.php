<?php

namespace app\controllers;

use yii\rest\ActiveController;

class CodeAgreementController extends ActiveController
{

    public $modelClass = 'app\models\Agreement';

    public function actions()
    {
        $actions = parent::actions();
        
        $actions['view'] = [
            'class' => 'app\components\codeAgreement\ViewAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
