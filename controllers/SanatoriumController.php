<?php

namespace app\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

class SanatoriumController extends ActiveController
{

    public $modelClass = 'app\models\Sanatorium';

    public function actions()
    {
        $actions = parent::actions();

        $actions['create'] = [
            'class' => 'app\components\sanatorium\SearchAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];
        
        $actions['view'] = [
            'class' => 'app\components\sanatorium\ViewAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
