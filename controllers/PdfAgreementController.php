<?php

namespace app\controllers;

use yii\rest\ActiveController;

class PdfAgreementController extends ActiveController
{

    public $modelClass = 'app\models\Agreement';

    public function actions()
    {
        $actions = parent::actions();
        
        $actions['view'] = [
            'class' => 'app\components\pdfAgreement\ViewAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
