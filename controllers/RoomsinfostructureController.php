<?php

namespace app\controllers;

use yii\rest\ActiveController;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

class RoomsinfostructureController extends ActiveController
{

    public $modelClass = 'app\models\Roomsinfostructure';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => 'app\components\roomsinfostructure\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
