<?php

namespace app\controllers;

use yii\rest\ActiveController;

class TerritoryController extends ActiveController
{

    public $modelClass = 'app\models\Territory';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => 'app\components\territory\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
