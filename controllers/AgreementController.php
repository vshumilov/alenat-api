<?php

namespace app\controllers;

use yii\rest\ActiveController;

class AgreementController extends ActiveController
{

    public $modelClass = 'app\models\Agreement';

    public function actions()
    {
        $actions = parent::actions();
        
        $actions['view'] = [
            'class' => 'app\components\agreement\ViewAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        $actions['create'] = [
            'class' => 'app\components\agreement\CreateAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
