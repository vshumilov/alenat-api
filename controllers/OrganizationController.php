<?php

namespace app\controllers;

use yii\rest\ActiveController;

class OrganizationController extends ActiveController
{

    public $modelClass = 'app\models\Organization';

    public function actions()
    {
        $actions = parent::actions();
        
        $actions['index'] = [
            'class' => 'app\components\organization\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
