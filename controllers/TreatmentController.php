<?php

namespace app\controllers;

use yii\rest\ActiveController;

class TreatmentController extends ActiveController
{

    public $modelClass = 'app\models\Treatment';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => 'app\components\treatment\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
