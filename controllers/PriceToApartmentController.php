<?php

namespace app\controllers;

use yii\rest\ActiveController;

class PriceToApartmentController extends ActiveController
{

    public $modelClass = 'app\models\PriceToApartment';

    public function actions()
    {
        $actions = parent::actions();

        $actions['view'] = [
            'class' => 'app\components\priceToApartment\ViewAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
