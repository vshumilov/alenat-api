<?php

namespace app\controllers;

use yii\rest\ActiveController;

class SmsController extends ActiveController
{

    public $modelClass = 'app\models\Sms';

    public function actions()
    {
        $actions = parent::actions();
        
        $actions['index'] = [
            'class' => 'app\components\sms\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        $actions['create'] = [
            'class' => 'app\components\sms\CreateAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }

}
