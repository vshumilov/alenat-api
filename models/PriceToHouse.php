<?php

namespace app\models;

class PriceToHouse extends BaseRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'prices_houses';
    }
    
    protected function getDefaultSelectFields() 
    {
        return ['house_id', 'price_id'];
    }
}

