<?php

namespace app\models;

use yii\db\ActiveRecord;

class SanatoriumAttr extends BaseRecord
{
    public function search($params = null)
    {
        $items = [];
        
        if (!empty($params['id'])) {
            $items = [$this->getById($params['id'])];
        } else if (!empty($params['name'])) {
            $items = $this->getList($params);
        }
        
        return $items;
    }
    
    public function getList($params = null) 
    {
        $query = self::find()->
        select($this->getDefaultSelectFields())->
        where(['deleted' => '0'])->
        andWhere(['!=', 'name', '']);
        
        if (!empty($params) && is_array($params)) {
            foreach ($params as $fieldName => $value) {
                if (!isset($value) || in_array($fieldName, ['limit'])) {
                    continue;
                }
                
                $query->andWhere(['like', $fieldName, $value]);
            }
        }
        
        if (!empty($params['limit'])) {
            $query->limit($params['limit']);
        }
        
        return $query->asArray()->all();
    }
    
    protected function getDefaultSelectFields()
    {
        return ['id', 'name', 'description'];
    }
}
