<?php

namespace app\models;

use yii\db\ActiveRecord;

class Treatment extends SanatoriumAttr
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'treatments';
    }
}

