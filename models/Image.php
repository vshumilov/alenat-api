<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "images".
 *
 * @property string $id
 * @property string $name
 * @property string $parent_id
 * 
 */
class Image extends BaseRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'images';
    }

    public function getList($parentId = null)
    {
        if (empty($parentId)) {
            return [];
        }

        return self::find()->
                        select([
                            'id',
                            'name',
                            'image_type',
                            'thumbnail_name_s',
                            'thumbnail_name_m',
                            'file_ext',
                            'order_number',
                            'parent_id']
                        )->
                        where(['deleted' => '0'])->
                        andWhere(['parent_id' => $parentId])->
                        orderBy('order_number')->
                        asArray()->all();
    }

}
