<?php

namespace app\models;

use yii;

class AlenatCrmApi
{

    private $token = null;
    private $url = null;
    private static $instance = null;
    private $cookiesFilePath = '/tmp/alenatCaches';

    private function __constrict()
    {
        
    }

    /**
     * @param string $url
     * @param string $token
     * @return self
     */
    public static function i($url = null, $token = null)
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        };

        if ($url) {
            self::$instance->url = $url;
        }

        if ($token) {
            self::$instance->token = $token;
        }
        return self::$instance;
    }

    /**
     * @param string $action
     * @param array $data
     * 
     * @return array|false
     */
    public function get($action, $data = [], $errorCode = false, $buildQuery = true)
    {
        $urlParams = http_build_query($data, '', '&', PHP_QUERY_RFC3986);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        curl_setopt($ch, CURLOPT_URL, $this->url . '/' . $action . '?' . $urlParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiesFilePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiesFilePath);

        $curlResult = curl_exec($ch);

        $info = curl_getinfo($ch);

        if ($errorCode) {
            return $info['http_code'];
        }

        if (!$curlResult) {
            return ['error' => curl_error($ch), 'error_code' => $info['http_code']];
        }
        
        curl_close($ch);

        return $curlResult;
    }

    public function post($action, $data = [], $errorCode = false, $buildQuery = true)
    {
        $urlParams = http_build_query($data, '', '&', PHP_QUERY_RFC3986);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

        if ($buildQuery) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($ch, CURLOPT_URL, $this->url . '/' . $action . '?' . $urlParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiesFilePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiesFilePath);

        $curlResult = curl_exec($ch);

        $info = curl_getinfo($ch);

        if ($errorCode) {
            return $info['http_code'];
        }

        if (!$curlResult) {
            return ['error' => curl_error($ch), 'error_code' => $info['http_code']];
        }
        
        curl_close($ch);

        return $curlResult;
    }

    public function put($action, $data = [], $errorCode = true)
    {
        $urlParams = http_build_query($data, '', '&', PHP_QUERY_RFC3986);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_URL, $this->url . '/' . $action . '?' . $urlParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiesFilePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiesFilePath);

        $curlResult = curl_exec($ch);
        $info = curl_getinfo($ch);

        if ($errorCode) {
            return $info['http_code'];
        }

        if (!$curlResult) {
            return ['error' => curl_error($ch), 'error_code' => $info['http_code']];
        }

        return $curlResult;
    }

    public function delete($action, $data = [])
    {
        $urlParams = http_build_query($data, '', '&', PHP_QUERY_RFC3986);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_URL, $this->url . '/' . $action . '?' . $urlParams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookiesFilePath);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookiesFilePath);

        $curlResult = curl_exec($ch);
        $info = curl_getinfo($ch);

        if (!$curlResult) {
            return ['error' => curl_error($ch), 'error_code' => $info['http_code']];
        }

        return $info['http_code'];
    }

    public function arrayToUrl($data)
    {
        $array = array();

        if (empty($data)) {
            return $array;
        }

        foreach ($data as $value) {
            $array[] = urlencode(base64_encode($value));
        }

        return $array;
    }

}
