<?php

namespace app\models;

use yii\db\ActiveRecord;

class City extends SanatoriumAutocompleteAttr
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'cities';
    }
}

