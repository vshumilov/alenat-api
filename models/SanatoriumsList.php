<?php

namespace app\models;

use yii\db\ActiveRecord;

class SanatoriumsList extends BaseRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'sanatoriums_list_view';
    }
    
    public function getList($params = null)
    {
        if (empty($params)) {
            return [];
        }

        $query = self::find();

        foreach ($params as $fieldName => $value) {
            if (empty($value) || empty($fieldName) || in_array($fieldName, ['limit', 'offset', 'order_by', 'group_by', 'order_mode', 'count'])) {
                continue;
            }

            if (is_array($value)) {
                foreach ($value as $subValue) {
                    $query->andWhere(['like', $fieldName, $subValue]);
                }
            } else if ($fieldName == 'date_from') {
                $query->andWhere(['>=', $fieldName, $value]);
            } else if ($fieldName == 'date_to') {
                $query->andWhere(['>=', $fieldName, $value]);
            } else {
                $query->andWhere(['like', $fieldName, $value]);
            }
        }
        
        if (!empty($params['group_by'])) {
            $query->groupBy($params['group_by']);
        }

        if (empty($params['count']) && !empty($params['order_by'])) {
            if (!empty($params['order_mode']) && $params['order_mode'] == 'desc') {
                $query->orderBy([$params['order_by'] => SORT_DESC]);
            } else {
                $query->orderBy([$params['order_by'] => SORT_ASC]);
            }
        }

        if (empty($params['count']) && !empty($params['offset'])) {
            $query->offset($params['offset']);
        }

        $limit = 20;

        if (!empty($params['limit'])) {
            $limit = $params['limit'];
        }

        if (empty($params['count'])) {
            $query->limit($limit);
        }

        if (empty($params['count'])) {
            return $query->asArray()->all();
        }

        return $query->asArray()->count();
    }
}

