<?php

namespace app\models;

class House extends BaseRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'houses';
    }
}

