<?php

namespace app\models;

class SanatoriumAutocompleteAttr extends SanatoriumAttr
{
    public function autocomplete($name = null) 
    {
        if (empty($name)) {
            return [];
        }
        
        $params = ['name' => $name, 'limit' => 5];
        
        return $this->getList($params);
    }
}
