<?php

namespace app\models;

use yii\db\ActiveRecord;

class Sport extends SanatoriumAttr
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'sports';
    }
}

