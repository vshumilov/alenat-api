<?php

namespace app\models;

use yii\db\ActiveRecord;

class Country extends SanatoriumAutocompleteAttr
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'countries';
    }
}

