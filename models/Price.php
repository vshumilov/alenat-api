<?php

namespace app\models;

class Price extends BaseRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'prices';
    }

    public function fields()
    {
        return [
            'id',
            'parent_id',
            'cost',
            'cost_with_treatment',
            'treatment_cost',
            'food_cost',
            'accommodation_cost',
            'date_from',
            'date_to',
            'foodoption_id',
            'pricelevel_id',
            'sanatorium',
            'foodoption',
            'houses'
        ];
    }

    public function getList($params = null)
    {
        if (empty($params) || empty($params['date_from']) || empty($params['date_to'])) {
            return [];
        }

        $currentDbDate = date("Y-m-d");

        $query = self::find()->
                select([
                    'id',
                    'parent_id',
                    'cost',
                    'cost_with_treatment',
                    'foodoption_id',
                    'pricelevel_id'
                ])->
                where(['deleted' => '0'])->
                andWhere(['<=', 'date_from', $params['date_from']])->
                andWhere(['>=', 'date_to', $currentDbDate]);

        if (!empty($params['foodoption'])) {
            $foodoptionWhere = ['or'];

            foreach ($params['foodoption'] as $foodoptionId => $foodoption) {
                $foodoptionWhere[] = ['foodoption_id' => $foodoptionId];
            }

            $query->andWhere($foodoptionWhere);
        }

        if (!empty($params['sanatoriums_ids'])) {
            $query->andWhere(['in', 'parent_id', $params['sanatoriums_ids']]);
        }

        return $query->asArray()->all();
    }

    protected function getByIdQuery($id)
    {
        $query = parent::getByIdQuery($id);

        $query->with('sanatorium');

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSanatorium()
    {
        return $this->hasOne(Sanatorium::className(), ['id' => 'parent_id'])->
                        where(['deleted' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFoodoption()
    {
        return $this->hasOne(Foodoption::className(), ['id' => 'foodoption_id'])->
                        where(['deleted' => '0']);
    }

    public function getHouses()
    {
        return $this->hasMany(House::className(), ['id' => 'house_id'])->
                        where(['deleted' => '0'])->
                        viaTable('prices_houses', ['price_id' => 'id'], function ($query) {
                            $query->andWhere(['deleted' => '0']);
                        })->orderBy('name');
    }

}
