<?php

namespace app\models;

use yii\db\ActiveRecord;

class Pricelevel extends SanatoriumAttr
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'pricelevels';
    }
    
    public function fields()
    {
        return [
            'id',
            'name'
        ];
    }
}

