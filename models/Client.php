<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\models\Entry;
use Yii;

class Client extends ActiveRecord implements IdentityInterface
{

    public $authKey;
    public $accessToken;
    protected $cookieTime = 2592000;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'password', 'birthdate'], 'required'],
            [['login', 'options'], 'string'],
            [['login', 'password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'password' => 'Password',
            'birthdate' => 'Birthdate',
        ];
    }

    /**
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields['password']);
        unset($fields['access_token']);

        return $fields;
    }

    public function beforeSave($insert)
    {
        $result = parent::beforeSave($insert);

        $clientArray = Client::find()->where(['login' => $this->login])->asArray()->one();

        if (!empty($this->password) && $this->password != $clientArray['password']) {
            $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }

        if ($this->isNewRecord) {
            $this->access_token = Yii::$app->getSecurity()->generatePasswordHash(rand(0, 1000));
        }

        return $result;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token, 'is_deleted' => false]);
    }

    public function markDeleted()
    {
        $this->login .= md5(rand(0, 1000));
        $this->is_deleted = true;
        $this->deleted = date('Y-m-d H:i:s', time());
        $result = $this->save();

        return $result;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$clients[$id]) ? new static(self::$clients[$id]) : null;
    }

    /**
     * Finds client by clientname
     *
     * @param string $clientname
     * @return static|null
     */
    public static function findByClientname($clientname)
    {
        foreach (self::$clients as $client) {
            if (strcasecmp($client['name'], $clientname) === 0) {
                return new static($client);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current client
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function login()
    {
        $result = \Yii::$app->client->login($this, $this->cookieTime);

        if (empty($result)) {
            return $result;
        }

        $entry = new Entry(['client_id' => \Yii::$app->client->id, 'ip' => \Yii::$app->request->getClientIP()]);
        $entry->save();

        return $result;
    }

}
