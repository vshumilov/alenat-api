<?php

namespace app\models;

use yii\db\ActiveRecord;

class Sanatorium extends SanatoriumAutocompleteAttr
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'sanatoriums';
    }

    public function fields()
    {
        return [
            'id',
            'int_id',
            'price_from',
            'country_id',
            'city_id',
            'thumbnail_name_s',
            'name',
            'lat',
            'lon',
            'treatment',
            'suitable',
            'healthprogram',
            'beauty',
            'roomsinfostructure',
            'entertainment',
            'territory',
            'internet',
            'rent',
            'sport',
            'city',
            'country'
        ];
    }

    public function getByIntId($intId)
    {
        if (empty($intId)) {
            return;
        }

        return $query = self::find()->
                        with('images')->
                        with('country')->
                        with('city')->
                        with('sanatoriumText')->
                        with('apartments')->
                        where(['deleted' => '0'])->
                        andWhere(['int_id' => $intId])->asArray()->one();
    }

    public function search($params = null)
    {
        if (empty($params) || empty($params['date_from']) || empty($params['date_to'])) {
            return !empty($params['count']) ? 0 : [];
        }

        $data = [];

        $country = new Country();
        $countryParams = ['id' => $params['country_id'], 'name' => $params['country']];
        $data['countries'] = $country->search($countryParams);
        $params['countries_ids'] = $country->getIdsList($data['countries']);

        $city = new City();
        $cityParams = ['id' => $params['city_id'], 'name' => $params['city']];
        $data['cities'] = $city->search($cityParams);
        $params['cities_ids'] = $city->getIdsList($data['cities']);

        if (!empty($params['name'])) {
            $data['sanatoriums'] = $this->getList($params);
            $params['sanatoriums_ids'] = $this->getIdsList($data['sanatoriums']);
        }

        $price = new Price();
        $data['prices'] = $price->getList($params);

        if (empty($data['prices'])) {
            return !empty($params['count']) ? 0 : [];
        }

        $params['prices_ids'] = $price->getIdsList($data['prices']);

        if (empty($params['name'])) {
            $params['sanatoriums_ids'] = $this->getIdsList($data['prices'], 'parent_id');
            $data['sanatoriums'] = $this->getList($params);
        }

        if (empty($data['sanatoriums'])) {
            return !empty($params['count']) ? 0 : [];
        }

        $apartment = new Apartment();
        $apartmentParams = ['places_count' => $params['places_count']];
        $data['apartments'] = $apartment->getList($apartmentParams);
        $params['apartments_ids'] = $this->getIdsList($data['apartments']);

        if (empty($data['apartments'])) {
            return !empty($params['count']) ? 0 : [];
        }

        $priceToApartment = new PriceToApartment();
        $priceToApartmentParams = ['price_id' => $params['prices_ids'], 'apartment_id' => $params['apartments_ids']];

        if (!empty($params['count'])) {
            $priceToApartmentParams['count'] = $params['count'];
        }

        $data['priceToApartments'] = $priceToApartment->getList($priceToApartmentParams);

        if (!empty($data['priceToApartments']) && !empty($params['count'])) {
            return $data['priceToApartments'];
        }

        $params['apartments_ids'] = $this->getIdsList($data['priceToApartments'], 'apartment_id');

        if (empty($data['priceToApartments'])) {
            return !empty($params['count']) ? 0 : [];
        }

        $priceToHouse = new PriceToHouse();
        $priceToHouseParams = ['price_id' => $params['prices_ids']];

        $data['priceToHouses'] = $priceToHouse->getList($priceToHouseParams);

        $house = new House();
        $houseParams = ['parent_id' => $params['sanatoriums_ids']];
        $data['houses'] = $house->getList($houseParams);

        return $data;
    }

    public function getList($params = null)
    {
        if (empty($params)) {
            return [];
        }

        $selectFields = [
            'id',
            'int_id',
            'lat',
            'lon',
            'price_from',
            'country_id',
            'city_id',
            'thumbnail_name_s'
        ];

        $searchableFields = [
            'name',
            'treatment',
            'suitable',
            'healthprogram',
            'beauty',
            'roomsinfostructure',
            'entertainment',
            'territory',
            'internet',
            'rent',
            'sport'
        ];

        $selectFields = array_merge($selectFields, $searchableFields);

        $query = self::find()->
                select($selectFields)->
                where(['!=', 'name', ''])->
                andWhere(['deleted' => '0']);

        if (!empty($params['country_id'])) {
            $query->andWhere(['country_id' => $params['country_id']]);
        } else if (!empty($params['countries_ids'])) {
            $query->andWhere(['in', 'country_id', $params['countries_ids']]);
        }

        if (!empty($params['city_id'])) {
            $query->andWhere(['city_id' => $params['city_id']]);
        } else if (!empty($params['cities_ids'])) {
            $query->andWhere(['in', 'city_id', $params['cities_ids']]);
        }

        if (!empty($params['sanatoriums_ids'])) {
            $query->andWhere(['in', 'id', $params['sanatoriums_ids']]);
        }

        foreach ($searchableFields as $fieldName) {
            if (empty($params[$fieldName]) || in_array($fieldName, $this->queryFields)) {
                continue;
            }

            if (is_array($params[$fieldName])) {
                foreach ($params[$fieldName] as $subValueId => $subValue) {
                    $query->andWhere(['like', $fieldName, $subValueId]);
                }
            } else {
                $query->andWhere(['like', $fieldName, $params[$fieldName]]);
            }
        }

        return $query->asArray()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])->
                        select(['id', 'name'])->
                        where([Country::tableName() . '.deleted' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id'])->
                        select(['id', 'name'])->
                        where([City::tableName() . '.deleted' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSanatoriumText()
    {
        return $this->hasOne(SanatoriumText::className(), ['sanatorium_id' => 'id'])->
                        select([
                            'id',
                            'sanatorium_id',
                            'date_modified',
                            'description'
                        ])->
                        where([SanatoriumText::tableName() . '.deleted' => '0']);
    }

    public function getImage()
    {
        return $this->hasOne(Image::className(), ['parent_id' => 'id'])->
                        select([
                            'id',
                            'name',
                            'thumbnail_name_s',
                            'thumbnail_name_m',
                            'file_ext',
                            'order_number'
                        ])->
                        where([Image::tableName() . '.deleted' => '0'])->
                        andWhere([Image::tableName() . '.is_avatar' => '1']);
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['parent_id' => 'id'])->
                        select([
                            'id',
                            'name',
                            'image_type',
                            'thumbnail_name_s',
                            'thumbnail_name_m',
                            'file_ext',
                            'order_number',
                            'parent_id'
                        ])->
                        where([Image::tableName() . '.deleted' => '0'])->
                        orderBy('order_number');
    }

    public function getApartments()
    {
        return $this->hasMany(Apartment::className(), ['parent_id' => 'id'])->
                        select([
                            'id',
                            'name',
                            'rooms_count',
                            'places_count',
                            'parent_id',
                            'pricelevel_id',
                            'roomsinfostructure',
                        ])->
                        where([Apartment::tableName() . '.deleted' => '0'])->
                        orderBy('places_count');
    }

}
