<?php

namespace app\models;

use yii\db\ActiveRecord;

class Entertainment extends SanatoriumAttr
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'entertainments';
    }
    
    protected function getDefaultSelectFields()
    {
        return ['id', 'name', 'description', 'is_for_children'];
    }
}

