<?php

namespace app\models;

use yii\db\ActiveRecord;

class BaseRecord extends ActiveRecord
{

    public $queryFields = ['limit', 'offset', 'order_by', 'order_mode', 'count'];

    public function fields()
    {
        return ['id', 'name'];
    }

    public static function shortClassName()
    {
        $className = self::className();

        return (new \ReflectionClass(new $className()))->getShortName();
    }

    public function getList($params = null)
    {
        $query = $this->getListQuery($params);

        return $query->asArray()->all();
    }

    protected function getListQuery($params = null)
    {
        $query = self::find()->
                select($this->getDefaultSelectFields())->
                where(['deleted' => '0']);

        if (!empty($params) && is_array($params)) {
            foreach ($params as $fieldName => $value) {
                if (empty($value) || in_array($fieldName, $this->queryFields)) {
                    continue;
                }

                if (is_array($value)) {
                    $query->andWhere(['in', $fieldName, $value]);
                } else {
                    $query->andWhere([$fieldName => $value]);
                }
            }
        }

        if (!empty($params['limit'])) {
            $query->limit($params['limit']);
        }

        return $query;
    }

    protected function getDefaultSelectFields()
    {
        return ['id', 'name'];
    }

    public function getById($id)
    {
        if (empty($id)) {
            return [];
        }

        return $this->getByIdQuery($id)->one();
    }

    protected function getByIdQuery($id)
    {
        return self::find()->
                        select($this->getDefaultSelectFields())->
                        where(['id' => $id])->
                        andWhere(['deleted' => '0']);
    }

    public function getIdsList($list, $fieldName = 'id')
    {
        $idsList = [];

        if (empty($list)) {
            return $idsList;
        }

        foreach ($list as $item) {
            if (in_array($item[$fieldName], $idsList)) {
                continue;
            }

            $idsList[] = $item[$fieldName];
        }

        return $idsList;
    }

}
