<?php

namespace app\models;

use yii\db\ActiveRecord;

class Suitable extends SanatoriumAttr
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'suitables';
    }
}

