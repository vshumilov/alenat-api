<?php

namespace app\models;

use yii\db\ActiveRecord;

class Healthprogram extends SanatoriumAttr
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return 'healthprograms';
    }
}

