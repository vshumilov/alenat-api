<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property string $id
 * @property string $name
 * @property string $date_entered
 * @property string $date_modified
 * @property string $description
 * @property boolean $deleted
 * @property string $sanatorium_id
 */
class SanatoriumText extends ActiveRecord
{
    public static function tableName()
    {
        return 'sanatoriumstexts';
    }
}

