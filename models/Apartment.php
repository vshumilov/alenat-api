<?php

namespace app\models;

class Apartment extends BaseRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'apartments';
    }
    
    public function fields()
    {
        return [
            'id',
            'name',
            'rooms_count',
            'places_count',
            'parent_id',
            'pricelevel_id',
            'pricelevel',
            'roomsinfostructure'
        ];
    }
    
    protected function getDefaultSelectFields()
    {
        return ['id', 'name', 'roomsinfostructure'];
    }
    
    protected function getByIdQuery($id)
    {
        $query = parent::getByIdQuery($id);
        
        $query->with('pricelevel');
        
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPricelevel()
    {
        return $this->hasOne(Pricelevel::className(), ['id' => 'pricelevel_id'])->
                        where(['deleted' => '0']);
    }
}

