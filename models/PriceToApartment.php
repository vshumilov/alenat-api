<?php

namespace app\models;

class PriceToApartment extends BaseRecord
{

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'prices_apartments';
    }
    
    public function fields()
    {
        return ['id', 'apartment', 'price'];
    }

    protected function getDefaultSelectFields()
    {
        return ['id', 'apartment_id', 'price_id'];
    }

    public function getList($params = null)
    {
        $query = $this->getListQuery($params);

        if (empty($params['count']) && !empty($params['order_by'])) {
            if (!empty($params['order_mode']) && $params['order_mode'] == 'desc') {
                $query->orderBy([$params['order_by'] => SORT_DESC]);
            } else {
                $query->orderBy([$params['order_by'] => SORT_ASC]);
            }
        }

        if (empty($params['count']) && !empty($params['offset'])) {
            $query->offset($params['offset']);
        }

        $limit = 20;

        if (!empty($params['limit'])) {
            $limit = $params['limit'];
        }

        if (empty($params['count'])) {
            $query->limit($limit);
        }

        if (empty($params['count'])) {
            return $query->asArray()->all();
        }

        return $query->asArray()->count();
    }

    protected function getByIdQuery($id)
    {
        $query = parent::getByIdQuery($id);
        
        $query->with('price')->
                with('apartment');
        
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrice()
    {
        return $this->hasOne(Price::className(), ['id' => 'price_id'])->
                        where(['deleted' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartment()
    {
        return $this->hasOne(Apartment::className(), ['id' => 'apartment_id'])->
                        where(['deleted' => '0']);
    }

}
